-- select 1 or 1, 1 or 0, 0 or 0, 0 or 1;
-- select 1 and 1, 1 and 0, 0 and 0, 0 and 1;
-- select 1 or null, 0 or null;
-- select 1 and null, 0 and null;
-- select count(distinct city) as num_city  from customers;
-- select distinct city from customers;
-- /* show details of customer who belongs to madrid, paris, las vegas*/
-- select * from customers where city = 'madrid' or city = 'paris' or city = 'las vegas';
-- select * from customers where city in ( 'madrid', 'paris','las vegas');
-- /* show details of customer who belongs to madrid, paris, las vegas and has credit limit more than 50000*/
-- select * from customers where (city = 'madrid' or city = 'paris' or city = 'las vegas') and (creditLimit > 50000);
-- select * from orders;
-- /* show order number who have ordered from 2003-01-01 to 2003-01-31*/
-- select orderNumber, status from orders where orderDate between '2003-01-01' and '2003-01-31';
-- select * from customers limit 10, 10;
-- select * from customers order by creditLimit limit 1, 1;
-- select * from customers;
-- /* write a query to show customername, phone where state is not null*/
-- select customername, phone from customers where state is not null;
-- select * from employees;
-- select concat(firstname,lastname) as full_name from employees;
-- select concat_ws('-',firstname,lastname,email) as full_name from employees;
-- select * from payments;
-- total payment, avg payment
-- select sum(amount) as total_amount, avg(amount) as average_amount from payments;
-- select e.firstname, e.lastname from employees e order by e.lastname;
/* customersorders count customername from customers and orders, sort it in descending */
-- select * from customers;
-- select * from orders;
-- select customers.customerName, count(*) as number_of_orders from customers left join orders on customers.customerNumber = orders.customerNumber group by customers.customerNumber order by  number_of_orders desc;
-- select customerName, count(ordernumber) as number_of_orders from customers inner join orders on customers.customerNumber = orders.customerNumber group by customers.customerNumber order by  number_of_orders desc;
-- select customerName, count(o.ordernumber) total from customers c inner join orders o on c.customerNumber = o.customerNumber group by customerName order by total desc;
-- from --> where --> group by --> select --> 
-- distinct -->  order by --> limit
select status from orders group by status;
select status,count(status) as count from orders group by status;
/* total amount of all orders by status */
select status, count(status), sum(quantityOrdered * priceEach) as total_amout from orders inner join orderdetails using (ordernumber) group by status;
-- select count(status) from orders;
select * from orderdetails;
-- select * from orders;
