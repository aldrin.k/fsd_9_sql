select * from customers;
select contactLastName, contactFirstName from customers order by contactLastName;
select contactLastName, contactFirstName from customers where contactLastname Like 's%' order by contactLastName;
select quantityOrdered, priceEach, quantityOrdered * priceEach as totalPrice from orderdetails order by totalPrice desc;
select * from employees where jobTitle = 'Sales Rep' and officeCode = 4;
/*select * from employees where employeeNumber > 1500;*/
/*select * from employees where employeeNumber between 1500 and 1800;*/
select distinct firstname,lastName from employees order by firstname;
select 1 or 1, 1 or 0, 0 or 0, 0 or 1;
select 1 and 1, 1 and 0, 0 and 0, 0 and 1;