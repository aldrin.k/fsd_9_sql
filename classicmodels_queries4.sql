-- create table sales select productline, 
-- year(orders.orderdate) as orderyear, 
-- sum(orderdetails.quantityOrdered * orderdetails.priceEach) as ordervalue
-- from orderdetails 
-- inner join orders 
-- using(ordernumber)
-- inner join products 
-- using(productcode)
-- group by productLine, orderyear;
-- select * from sales;
-- -- select * from products;
-- -- drop table sales;

select productline, orderyear, sum(ordervalue) as totalordervalue
from sales
group by
productline,
orderyear
with rollup;

/*write a query to return the customer who
 has done the highest payment*/
select c.customerName,p.amount,c.customernumber from 
customers c inner join payments p using(customernumber) 
order by p.amount desc limit 1;

select customernumber, checknumber, amount
from payments 
where amount = (select max(amount) 
from payments);

/* query to get productname whose buy price are greater than
average buy price*/