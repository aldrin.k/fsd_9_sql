-- /* extract year data from orderdate who's status are shipped and get the sum of total _amount according to year  */
-- /* select * from orders; select * from orderdetails; */
-- select year(orderdate) as year, sum(quantityordered * priceeach) 
-- from orders inner join orderdetails on orders.ordernumber = orderdetails.orderNumber 
-- where status = 'shipped' group by year;

-- select year(orderdate) as year, sum(quantityordered * priceeach) 
-- as shipped_amout from orders inner join orderdetails using(orderNumber) 
-- where status = 'shipped' group by year;

-- select year(orderdate) as year, sum(quantityordered * priceeach)
-- as shipped_amout from orders inner join orderdetails using(orderNumber) 
-- where status = 'shipped' and year(orderdate) > 2003 group by year;

-- select year(orderdate) as year, sum(quantityordered * priceeach) as 
-- shipped_amout from orders inner join orderdetails using(orderNumber) 
-- where status = 'shipped' group by year having year > 2003;

-- /* count number of orders shipped according to year*/
-- select year(orderDate) as year, count(ordernumber) as count_shipped_orders from orders where status = 'shipped' group by year;

-- select state from customers group by state; 
-- select distinct state from customers;

select * from products;
select distinct count(productline) as total_dis_product from productlines;
select count(productline) as total_dis_product from productlines group by productline;

