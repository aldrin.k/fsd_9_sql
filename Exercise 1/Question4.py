# Retrive names and id whose age is less than 30. 
import mysql.connector as mysql

# conn = mysql.connect(
#     host="localhost",user="root",password="aldrin", database='employee')
# cur = conn.cursor()
# cur.execute("select name, id from employee_details where DOB > '1983-01-04'")
# data = cur.fetchall()
# print(data)
# conn.close()

conn = mysql.connect(
    host="localhost",user="root",password="aldrin", database='employee')
cur = conn.cursor()
cur.execute("select name, id from employee_details where year(CURDATE()) - year(DOB) < 30;")
data = cur.fetchall()
print(data)
conn.close()
