#1. Create an employee table,  
import mysql.connector as mysql

# def creating_database(host,user,password):
#     conn = mysql.connect(host=host,user=user,password=password)
#     cursor = conn.cursor()
#     cursor.execute("CREATE DATABASE EMPLOYEE")
#     conn.close()
#     return 'Executed !!'
# print(creating_database('localhost','root','aldrin'))

# add columns(name, id, DOB, DOJ, Salary)

# conn = mysql.connect(host="localhost",user="root",password="aldrin", database='employee')
# cursor = conn.cursor()
# cursor.execute("create table employee_details (name varchar(255), id int, DOB date, DOJ date, Salary int);")
# conn.close()

# Add 10 records to the table as well
conn = mysql.connect(host="localhost",user="root",password="aldrin", database="employee")
cursor = conn.cursor()
cursor.execute('''INSERT INTO employee_details VALUES
('abc', 1, '1995-12-01', '2020-05-09', 50000),
('efg', 2, '1994-10-01', '2020-05-09', 60000),
('ijk', 3, '1980-12-01', '2020-05-09', 70000),
('lmn', 4, '1988-04-01', '2020-05-09', 55000),
('opq', 5, '1992-12-01', '2020-05-09', 45000),
('rst', 6, '2000-06-01', '2020-05-09', 30000),
('uvw', 7, '2001-12-01', '2020-05-09', 28000),
('xyz', 8, '2002-11-01', '2020-05-09', 85000),
('abc1', 9, '1997-10-01', '2020-05-09', 75000),
('abc2', 10, '1996-11-30', '2020-05-09', 55000);''')
conn.commit()
conn.close()

