import mysql.connector as mysql
data_ = {'name':['abcde', 'qwert', 'tyui', 'asdf', 'oiuy'],
'id':[11,12,13,14,15],'DOB':["1998-12-12", "1995-02-12","1999-12-10","2000-09-15","1998-12-12"],
'DOJ':['2020-05-09','2020-05-09','2020-05-09','2020-05-09','2020-05-09'], 
'SALARY':[55000, 45000, 36000, 78000, 50000]}

def update_records(host, user, password, database, records, data):
    data = data
    conn = mysql.connect(host=host,user=user,password=password,database=database)
    cur = conn.cursor()
    for row, i in enumerate(range(1,records+1)):
        cur.execute(f'''update employee_details set name='{data['name'][row]}',
        id={data['id'][row]},
        DOB = '{data['DOB'][row]}', 
        DOJ= '{data['DOJ'][row]}', 
        SALARY={data['SALARY'][row]} WHERE id = {i};''')
    conn.commit()
    conn.close()
    return 'Executed'
print(update_records("localhost","root","aldrin","employee", 5, data_))